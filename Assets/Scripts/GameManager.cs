using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoSingleton<GameManager>
{
	public enum GameState
	{
		init = 0,
		start,
		play,
		disconnect,
		finish
	}

	public enum GameResult
	{
		none = 0,
		win,
		lose,
	}
	
	[HideInInspector]
	public List<GameObject> foods = new List<GameObject> ();

	[HideInInspector]
	public GameState State;

	[HideInInspector]
	public GameResult Result;
	
	[Header ("Fish")]
	public GameObject redFishPrefab;
	public GameObject blueFishPrefab;
	public float fishTapForce = 50.0f;
	public float fishMaxSpeed = 4.5f;
	public float fishMinSpeed = 1.0f;
	
	public float fishInitSize = 1.0f;
	public float fishMinSize = 0.5f;
	public float fishMaxSize = 2.5f;
	public float foodBonus = 0.05f;
	public float baitPenalty = 0.05f;
	public float eatableSizeGap = 0.5f;
	public float eatableLimit = 1.5f;

	[Header ("Food")]
	public GameObject foodPrefab;
	public float foodLifetime = 5.0f;
	public float foodWaveIntervalRangeMin = 2.0f;
	public float foodWaveIntervalRangeMax = 4.0f;
	public int foodWaveCountRangeMin = 1;
	public int foodWaveCountRangeMax = 6;
	public float baitRate = 0.25f;

	public float foodGravity = 600.0f;
	public float foodDampingFood = 12.0f;
	public float foodDampingVelocityLimit = 3.0f;

    public void StartThrowFood()
    {
		if (Network.isServer)
		{
			StartCoroutine(ThrowFood(1.0f));
		}
    }

	IEnumerator ThrowFood(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		int count = Random.Range(foodWaveCountRangeMin, foodWaveCountRangeMax);
		int firstCount = count;
		while (count > 0) {
			count--;
			
			lock(foods)
			{
				float xLeft = (12.0f / firstCount * count) - 6.0f;
				float xRight = (12.0f / firstCount * (count + 1)) - 6.0f;
				GameObject newfood = (GameObject)Network.Instantiate(foodPrefab, new Vector3(Random.Range(xLeft, xRight), Random.Range(6.25f, 8.0f)), Quaternion.identity, 1);
				foods.Add(newfood);
			}
		}
		if (State == GameState.play)
			StartCoroutine(ThrowFood(Random.Range(foodWaveIntervalRangeMin, foodWaveIntervalRangeMax)));
	}
	
	void Update()
	{		
		if (State == GameState.play && Network.isServer) 
		{
			//		Rect fishRect = new Rect (fish.transform.position.x - fish.Strength/2f, fish.transform.position.y - fish.Strength/2f, fish.Strength, fish.Strength);
			//		
			lock (foods) {
				for (int i = foods.Count - 1; i >= 0; i--) {
					foods[i].GetComponent<Food>().Duration -= Time.deltaTime;
					if (foods[i].GetComponent<Food>().Duration < 0.0f)
					{
						Network.Destroy (foods [i].gameObject);
						foods.Remove (foods [i]);
					}
					//				else if (fishRect.Contains (foods [i].transform.position)) {
					//					fish.Strength += foods [i].GetComponent<Food>().IsHarmful ? -0.05f : 0.05f;
					//					Destroy (foods [i].gameObject);
					//					foods.Remove (foods [i]);
					//						}
				}
			}
		}        

	}
}
