﻿using UnityEngine;
using System.Collections;

public class Fish : MonoBehaviour {
	// variables for syn data
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;

	float previousVeloX = -1.0f;

	float initScale = 1.2f;
	float strength = 1.0f;
	int dirSign = 1;

	[RPC]
	public void AddStrength (float value)
	{
		Strength += value;
	}

	[RPC]
	public void GetEaten ()
	{
		gameObject.SetActive (false);
		GameManager.Instance.Result = GameManager.GameResult.lose;
		GameManager.Instance.State = GameManager.GameState.finish;
	}

	public float Strength {
		get {
			return strength;
		}
		set {
			if (strength != value)
			{
				strength = Mathf.Clamp(value, GameManager.Instance.fishMinSize, GameManager.Instance.fishMaxSize);

				transform.localScale = new Vector3(-dirSign * initScale, initScale) * strength;
				GetComponentInChildren<Animator>().SetFloat("Strength", strength);
				Debug.Log(strength);
			}
		}
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		float syncStrength = 1.0f;
		int syncDirSync = 0;

		if (stream.isWriting)
		{
			syncPosition = new Vector3(GetComponent<Rigidbody2D>().position.x, GetComponent<Rigidbody2D>().position.y, 0);
			stream.Serialize(ref syncPosition);
			
			syncVelocity =  new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0);
			stream.Serialize(ref syncVelocity);

			syncStrength = strength;
			stream.Serialize(ref syncStrength);

			syncDirSync = dirSign;
			stream.Serialize(ref syncDirSync);
		}
		else
		{
			stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncVelocity);
			stream.Serialize(ref syncStrength);
			stream.Serialize(ref syncDirSync);
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
			
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = GetComponent<Rigidbody2D>().position;

			Strength = syncStrength;
			dirSign = syncDirSync;
		}
	}
	
	void Awake()
	{
		lastSynchronizationTime = Time.time;
		GetComponent<NetworkView> ().observed = this;
	}

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D> ().AddForce(new Vector2 (80, 20));
		Strength = GameManager.Instance.fishInitSize;
	}
	
	// Update is called once per frame
	void Update () {
//		if ((Network.isServer && !GetComponent<NetworkView>().isMine) || (Network.isClient && GetComponent<NetworkView>().isMine))
//			GetComponentInChildren<SpriteRenderer>().color =   new Color(1.0f, 0.5f, 0.5f);
//		else
//			GetComponentInChildren<SpriteRenderer>().color =   new Color(1.0f, 1.0f, 1.0f);

		if (!GetComponent<NetworkView>().isMine)
		{
			SyncedMovement();
			//determine the sprite direction
			transform.localScale = new Vector3(-dirSign * Mathf.Abs(transform.localScale.x), transform.localScale.y);
		}
		else
		{
			//clamp velocity
			if (GetComponent<Rigidbody2D> ().velocity.magnitude < GameManager.Instance.fishMinSpeed)
			{
				GetComponent<Rigidbody2D> ().velocity = GetComponent<Rigidbody2D> ().velocity.normalized * GameManager.Instance.fishMinSpeed;
			}
			
			if (GetComponent<Rigidbody2D> ().velocity.magnitude > GameManager.Instance.fishMaxSpeed)
			{
				GetComponent<Rigidbody2D> ().velocity = GetComponent<Rigidbody2D> ().velocity.normalized * GameManager.Instance.fishMaxSpeed;
			}
			
			//receive user force
			if (Input.GetMouseButton(0) && GameManager.Instance.State == GameManager.GameState.play)
			{
				Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				GetComponent<Rigidbody2D> ().AddForce(-new Vector2 (this.transform.position.x - screenRay.origin.x, this.transform.position.y - screenRay.origin.y).normalized * GameManager.Instance.fishTapForce);
			}
			
			//make sure the sprite don't move out of screen bound
			if ( Mathf.Abs(transform.position.x) >= 6.0f && transform.position.x * GetComponent<Rigidbody2D> ().velocity.x > 0.0f)
			{
				GetComponent<Rigidbody2D> ().velocity = new Vector2(-GetComponent<Rigidbody2D> ().velocity.x / 2.0f, GetComponent<Rigidbody2D> ().velocity.y );
			}
			
			if ( Mathf.Abs(transform.position.y) >= 4.0f && transform.position.y * GetComponent<Rigidbody2D> ().velocity.y > 0.0f)
			{
				GetComponent<Rigidbody2D> ().velocity = new Vector2(GetComponent<Rigidbody2D> ().velocity.x, -GetComponent<Rigidbody2D> ().velocity.y / 2.0f);
			}
			//
			
			//determine the sprite direction
			if (previousVeloX * GetComponent<Rigidbody2D> ().velocity.x < 0.0f) 
			{
				dirSign = GetComponent<Rigidbody2D> ().velocity.x != 0 ? GetComponent<Rigidbody2D> ().velocity.x < 0 ? -1 : 1 : 1;
				transform.localScale = new Vector3(-dirSign * Mathf.Abs(transform.localScale.x), transform.localScale.y);
			}
			previousVeloX = GetComponent<Rigidbody2D> ().velocity.x;
		}

		if (Network.isServer) 
		{
			Rect fishRect = new Rect (transform.position.x - Strength / 2f, transform.position.y - Strength / 2f, Strength, Strength);

			for (int i = GameManager.Instance.foods.Count - 1; i >= 0; i--) {
				if (fishRect.Contains (GameManager.Instance.foods [i].transform.position)) {
					if (GetComponent<NetworkView>().isMine)
						Strength += GameManager.Instance.foods [i].GetComponent<Food>().IsHarmful ? -GameManager.Instance.baitPenalty : GameManager.Instance.foodBonus;
					else
						GetComponent<NetworkView>().RPC("AddStrength", RPCMode.OthersBuffered, GameManager.Instance.foods [i].GetComponent<Food>().IsHarmful ? -GameManager.Instance.baitPenalty : GameManager.Instance.foodBonus);
					Network.Destroy (GameManager.Instance.foods [i].gameObject);
					GameManager.Instance.foods.Remove (GameManager.Instance.foods [i]);
				}
			}
		}
	}

	/// <summary>
	/// Synceds the movement.
	/// </summary>
	private void SyncedMovement()
	{
		syncTime += Time.deltaTime;
		GetComponent<Rigidbody2D>().position = Vector2.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (this.GetComponent<NetworkView> ().isMine) 
		{
			Vector2 dist = other.transform.position - transform.position;
			if (other.GetComponent<Fish>() != null)
			{
				Debug.Log(dist.x + " " + GetComponent<Rigidbody2D>().velocity.x);
				if (((GetComponent<Fish>().Strength - GameManager.Instance.eatableSizeGap >= other.GetComponent<Fish>().Strength && GetComponent<Fish>().Strength >= GameManager.Instance.eatableLimit) || GetComponent<Fish>().Strength == GameManager.Instance.fishMaxSize) && dist.x * GetComponent<Rigidbody2D>().velocity.x > 0.0f)
				{
					GameManager.Instance.Result = GameManager.GameResult.win;
					GameManager.Instance.State = GameManager.GameState.finish;
					other.gameObject.SetActive (false);
					other.GetComponent<NetworkView>().RPC("GetEaten", RPCMode.OthersBuffered);
				}
				else
				{
					float x =  GetComponent<Rigidbody2D>().velocity.x * dist.x > 0 ? -GetComponent<Rigidbody2D>().velocity.x : GetComponent<Rigidbody2D>().velocity.x;
					float y =  GetComponent<Rigidbody2D>().velocity.y * dist.y > 0 ? -GetComponent<Rigidbody2D>().velocity.y : GetComponent<Rigidbody2D>().velocity.y;
					GetComponent<Rigidbody2D> ().velocity = new Vector2 (x, y);
				}
			}
		}
	}
}
