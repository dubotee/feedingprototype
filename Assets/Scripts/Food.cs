﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {
	// variables for syn data
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;
	private int syncType;

	public Sprite[] variant;
	bool isDamping = false;
	float originY = 0.0f;

	float lerpsmoothZAngleValue = 0.0f;
	float randomZAngleValue = 0.0f;
	float prevRandomZAngleValue = 0.0f;
	
	float duration = 5.0f;
	bool isHarmful = false;
	int type;

	public float Duration {
		get {
			return duration;
		}
		set {
			duration = value;
		}
	}

	public bool IsHarmful {
		get {
			return isHarmful;
		}
	}
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		Vector3 syncPosition = new Vector3(0.0f, 7.0f);
		Vector3 syncVelocity = Vector3.zero;
		if (stream.isWriting)
		{
			syncPosition = new Vector3(GetComponent<Rigidbody2D>().position.x, GetComponent<Rigidbody2D>().position.y, 0);
			stream.Serialize(ref syncPosition);
			
			syncVelocity =  new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0);
			stream.Serialize(ref syncVelocity);

			syncType = type;
			stream.Serialize(ref syncType);
		}
		else
		{
			stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncVelocity);
			stream.Serialize(ref syncType);
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
			
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = GetComponent<Rigidbody2D>().position;
			if (type != syncType)
			{
				type = syncType;
				GetComponent<SpriteRenderer> ().sprite = variant [type];
			}
		}
	}

	void Awake()
	{
		lastSynchronizationTime = Time.time;
		GetComponent<NetworkView> ().observed = this;
	}

	// Use this for initialization
	void Start () {
		originY = Random.Range (-2.5f, 2.5f);
//		GetComponent<Rigidbody2D>().position = new Vector2 (Random.Range (-4.0f, 4.0f), Random.Range(6.25f, 8.0f));

		randomZAngleValue = Random.Range (-20f, 20f);
		prevRandomZAngleValue = 0;
		duration = GameManager.Instance.foodLifetime;

		if (variant.Length == 0) {
			return;
		}
		float randomRate = Random.Range (0.0f, 1.0f);
		if (randomRate < GameManager.Instance.baitRate) {
			type = variant.Length - 1;
			isHarmful = true;
		}
		else
		{
			type = Random.Range (0, variant.Length-1);
			isHarmful = false;
		}
		GetComponent<SpriteRenderer> ().sprite = variant [type];
	}
	
	// Update is called once per frame
	void Update () {
		if (!GetComponent<NetworkView>().isMine)
		{
			SyncedMovement();
		}
		else
		{
			//Apply damping force when the bait fall under origin y
			if (GetComponent<Rigidbody2D> ().position.y < originY)
				isDamping = true;
			
			if (isDamping)
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2( 0, Time.deltaTime * GameManager.Instance.foodGravity * GameManager.Instance.foodDampingFood * (originY - GetComponent<Rigidbody2D>().position.y)));
				if (GetComponent<Rigidbody2D>().velocity.magnitude > GameManager.Instance.foodDampingVelocityLimit)
				{
					GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * GameManager.Instance.foodDampingVelocityLimit;
				}
			}
			GetComponent<Rigidbody2D>().AddForce(-new Vector2( 0, Time.deltaTime * GameManager.Instance.foodGravity * GetComponent<Rigidbody2D>().position.y));

		}

		lerpsmoothZAngleValue += Time.deltaTime * 6.0f;
		if (lerpsmoothZAngleValue > 1.0f) 
		{
			prevRandomZAngleValue = randomZAngleValue;
			randomZAngleValue = Random.Range (-20f, 20f);
			lerpsmoothZAngleValue = 0.0f;
		}

		//Shake the bait
		transform.rotation = Quaternion.Euler(new Vector3 (0f, 0f, Mathf.Lerp(prevRandomZAngleValue, randomZAngleValue, lerpsmoothZAngleValue)) );
	}
	
	/// <summary>
	/// Synceds the movement.
	/// </summary>
	private void SyncedMovement()
	{
		syncTime += Time.deltaTime;
		GetComponent<Rigidbody2D>().position = Vector2.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}
}
