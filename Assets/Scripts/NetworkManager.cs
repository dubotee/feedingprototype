using UnityEngine;
using System.Collections;

public class NetworkManager : MonoSingleton<NetworkManager>
{
	private const string typeName = "gloops.com.FeedingPrototype";
	private const string gameName = "room1";
	
	private bool isRefreshingHostList = false;
	// contain all host
	private HostData[] hostList;
	
	// game prefabs
//	public GameObject playerPrefab;
//	public GameObject avatarServer;
//	public GameObject avatarClient;
	
	void OnGUI()
	{
		if (!Network.isClient && !Network.isServer)
		{
			if (GUI.Button(new Rect(30, 30, 100, 60), "Start Server"))
				StartServer();
			
			if (GUI.Button(new Rect(30, 120, 100, 60), "Refresh Hosts"))
				RefreshHostList();
			
			if (hostList != null)
			{
				for (int i = 0; i < hostList.Length; i++)
				{
					if (GUI.Button(new Rect(200, 30 + (110 * i), 100, 60), hostList[i].gameName))
					{
						JoinServer(hostList[i]);
					}
				}
			}
		}

		if (Network.isServer && GameManager.Instance.State == GameManager.GameState.init) {
			GUI.Label(new Rect(30, 30, 200, 60), "Waiting for opponent...");
		}

		if (GameManager.Instance.State == GameManager.GameState.finish)
		{
			GUI.Label(new Rect(30, 30, 100, 60), GameManager.Instance.Result != GameManager.GameResult.none ? GameManager.Instance.Result == GameManager.GameResult.win ? "You Win" : "You Lose" : "...");
			
			if (GUI.Button(new Rect(30, 120, 100, 60), "Restart"))
			{
				Network.Disconnect();
				Application.LoadLevel("Dummy");
			}
				
		}
	}
	
	private void StartServer()
	{
		Network.InitializeServer(2, 25000, !Network.HavePublicAddress());//
		MasterServer.RegisterHost(typeName, gameName);
	}

	void OnDisconnectedFromServer(NetworkDisconnection info) {
		Application.LoadLevel("Dummy");
	}

	void OnPlayerDisconnected(NetworkPlayer player) {
		if (Network.isServer) {
			Network.Disconnect();
			MasterServer.UnregisterHost();
		}
		Application.LoadLevel("Dummy");
	}

	void OnServerInitialized()
	{
		SpawnAvatar();
	}
	
	
	void Update()
	{
		if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
		{
			isRefreshingHostList = false;
			hostList = MasterServer.PollHostList();
		}
	}
	
	private void RefreshHostList()
	{
		if (!isRefreshingHostList)
		{
			isRefreshingHostList = true;
			MasterServer.RequestHostList(typeName);
		}
	}
	
	private void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
	}
	
	void OnConnectedToServer()
	{
		SpawnAvatar();
		GameManager.Instance.State = GameManager.GameState.play;
	}

	private void SpawnAvatar()
	{
		if (Network.isServer) 
		{
			GameObject go = Network.Instantiate (GameManager.Instance.redFishPrefab, new Vector3 (-2f, 0f, 0), Quaternion.identity, 0) as GameObject;
//			GameManager.Instance.StartThrowFood();
		}
		else
		{
			GameObject go = Network.Instantiate (GameManager.Instance.blueFishPrefab, new Vector3 (2, 0f, 0), Quaternion.identity, 0) as GameObject;
		}
	}
	
	//Behaviour method
	void OnPlayerConnected(NetworkPlayer player)
	{
		if (Network.isServer)
		{
			GameManager.Instance.State = GameManager.GameState.play;
			GameManager.Instance.StartThrowFood();
		}
	}
}
